<!doctype html>
<html lang="es">
    <head>
        <meta http-equiv="refresh" content="3;url=/" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>RaspRemote</title>
        <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap/css/bootstrap.min.css">
    </head>
    <body>
        <main class="container">
            <div class="jumbotron">
                <h1><?php echo $mensaje; ?></h1>
            </div>
        </main>
        <script type="text/javascript" src="/assets/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="/assets/plugins/popper/popper.min.js"></script>
        <script type="text/javascript" src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>

