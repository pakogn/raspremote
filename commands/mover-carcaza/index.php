<?php
require __DIR__.'/../../vendor/autoload.php';

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

$process = new Process('sudo python rutina.py');
$process->run();

if (!$process->isSuccessful()) {
    throw new ProcessFailedException($process);
}

echo $process->getOutput();


$mensaje = "Moviendo Carcaza";

include __DIR__.'/../mensaje.php';
?>
