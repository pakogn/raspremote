<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>RaspRemote</title>
        <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap/css/bootstrap.min.css">
    </head>
    <body>
        <main class="container">
            <div class="jumbotron">
                <h1>SISTEMA DE MONITOREO VÍA REMOTA PARA PROCESO AUTOMÁTICO DE BRAZOS ROBÓTICOS</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".AboutModal">Acerca de...</button>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div style="background-color: #eee; height: 450px"></div>
                </div>
                <div class="col-md-6 text-center">
                    <ul class="list-group">
                        <li class="list-group-item"><a href="commands/mover-piston/" class="btn btn-lg btn-primary">Mover pistón</a></li>
                        <li class="list-group-item"><a href="commands/mover-carcaza/" class="btn btn-lg btn-primary">Mover carcaza</a></li>
                        <li class="list-group-item"><a href="commands/hacer-circulo/" class="btn btn-lg btn-primary">Hacer un círculo</a></li>
                        <li class="list-group-item"><a href="commands/hacer-cuadrado/" class="btn btn-lg btn-primary">Hacer un cuadrado</a></li>
                    </ul>
                </div>
            </div>
        </main>
        <div class="modal fade AboutModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Acerca de...</h4>
                    </div>
                    <div class="modal-body">
                        <p>Desarrollado por:</p>
                        <ul>
                            <li>Juan Carlos</li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="/assets/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="/assets/plugins/popper/popper.min.js"></script>
        <script type="text/javascript" src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
